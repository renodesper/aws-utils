package main

import (
	"context"
	"fmt"
	"log"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/service/iam"
	"github.com/aws/aws-sdk-go-v2/service/iam/types"
)

type DuplicateIamRole struct {
	Client *iam.Client
}

func (d *DuplicateIamRole) GetRole(ctx context.Context, roleName string) *iam.GetRoleOutput {
	roleInput := iam.GetRoleInput{
		RoleName: &roleName,
	}
	sourceRole, err := d.Client.GetRole(ctx, &roleInput)
	if err != nil {
		log.Fatalf("failed to get role, %v", err)
	}

	return sourceRole
}

func (d *DuplicateIamRole) GetInlinePolicies(ctx context.Context, roleName string) []*iam.GetRolePolicyOutput {
	inlinePolicyNames := d.GetInlinePoliciesRecursive(ctx, roleName, "")

	if len(inlinePolicyNames) == 0 {
		return []*iam.GetRolePolicyOutput{}
	}

	var inlinePolicies []*iam.GetRolePolicyOutput

	for _, policyName := range inlinePolicyNames {
		rolePolicyInput := iam.GetRolePolicyInput{
			RoleName:   &roleName,
			PolicyName: &policyName,
		}

		inlinePolicy, err := d.Client.GetRolePolicy(ctx, &rolePolicyInput)
		if err != nil {
			log.Fatalf("failed to get role policy, %v", err)
		}

		inlinePolicies = append(inlinePolicies, inlinePolicy)
	}

	return inlinePolicies
}

func (d *DuplicateIamRole) GetInlinePoliciesRecursive(ctx context.Context, roleName string, marker string) []string {
	params := iam.ListRolePoliciesInput{
		RoleName: &roleName,
	}

	if marker != "" {
		params.Marker = &marker
	}

	rolePolicies, err := d.Client.ListRolePolicies(ctx, &params)
	if err != nil {
		log.Fatalf("failed to get list of role policies, %v", err)
	}

	inlinePolicyNames := rolePolicies.PolicyNames

	if rolePolicies.IsTruncated {
		inlinePolicyNames_ := d.GetInlinePoliciesRecursive(ctx, roleName, *rolePolicies.Marker)
		inlinePolicyNames = append(inlinePolicyNames, inlinePolicyNames_...)
	}

	return inlinePolicyNames
}

func (d *DuplicateIamRole) GetManagedPolicies(ctx context.Context, roleName string) []types.AttachedPolicy {
	managedPolicies := d.GetManagedPoliciesRecursive(ctx, roleName, "")
	return managedPolicies
}

func (d *DuplicateIamRole) GetManagedPoliciesRecursive(ctx context.Context, roleName string, marker string) []types.AttachedPolicy {
	params := iam.ListAttachedRolePoliciesInput{
		RoleName: &roleName,
	}

	if marker != "" {
		params.Marker = &marker
	}

	attachedRolePolicies, err := d.Client.ListAttachedRolePolicies(ctx, &params)
	if err != nil {
		log.Fatalf("failed to get list of attached role policies, %v", err)
	}

	managedPolicies := attachedRolePolicies.AttachedPolicies

	if attachedRolePolicies.IsTruncated {
		managedPolicies_ := d.GetManagedPoliciesRecursive(ctx, roleName, *attachedRolePolicies.Marker)
		managedPolicies = append(managedPolicies, managedPolicies_...)
	}

	return managedPolicies
}

func (d *DuplicateIamRole) CreateRole(ctx context.Context, sourceRole *iam.GetRoleOutput, targetRoleName string) error {
	params := iam.CreateRoleInput{
		Path:               sourceRole.Role.Path,
		RoleName:           &targetRoleName,
		Description:        sourceRole.Role.Description,
		MaxSessionDuration: sourceRole.Role.MaxSessionDuration,
		Tags:               sourceRole.Role.Tags,
	}

	assumeRolePolicyDocument, err := url.PathUnescape(*sourceRole.Role.AssumeRolePolicyDocument)
	if err != nil {
		return err
	}

	params.AssumeRolePolicyDocument = &assumeRolePolicyDocument

	if sourceRole.Role.PermissionsBoundary != nil {
		params.PermissionsBoundary = sourceRole.Role.PermissionsBoundary.PermissionsBoundaryArn
	} else {
		params.PermissionsBoundary = nil
	}

	_, err = d.Client.CreateRole(ctx, &params)
	if err != nil {
		return err
	}

	return nil
}

func (d *DuplicateIamRole) AttachInlinePolicies(ctx context.Context, targetRoleName string, inlinePolicies []*iam.GetRolePolicyOutput) error {
	for _, policy := range inlinePolicies {
		params := iam.PutRolePolicyInput{
			RoleName:   &targetRoleName,
			PolicyName: policy.PolicyName,
		}

		policyDocument, err := url.PathUnescape(*policy.PolicyDocument)
		if err != nil {
			params.PolicyDocument = &policyDocument
		}

		_, err = d.Client.PutRolePolicy(ctx, &params)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to add inline policy, %v", err))
		}
	}

	return nil
}

func (d *DuplicateIamRole) AttachManagedPolicies(ctx context.Context, targetRoleName string, managedPolicies []types.AttachedPolicy) error {
	for _, policy := range managedPolicies {
		params := iam.AttachRolePolicyInput{
			RoleName:  &targetRoleName,
			PolicyArn: policy.PolicyArn,
		}

		_, err := d.Client.AttachRolePolicy(ctx, &params)
		if err != nil {
			fmt.Println(fmt.Errorf("failed to add managed policy, %v", err))
		}
	}

	return nil
}
