package main

import (
	"context"
	"flag"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/iam"
)

func main() {
	sourceRoleName := flag.String("source", "", "role name that we want to use as a source")
	targetRoleName := flag.String("target", "", "role name that we want to create")
	flag.Parse()

	if *sourceRoleName == "" {
		log.Fatalf("source argument cannot be empty")
		return
	}

	if *targetRoleName == "" {
		log.Fatalf("target argument cannot be empty")
		return
	}

	// Using the SDK's default configuration, loading additional config
	// and credentials values from the environment variables, shared
	// credentials, and shared configuration files
	ctx := context.Background()
	cfg, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		log.Fatalf("unable to load SDK config, %v", err)
		return
	}

	iamClient := iam.NewFromConfig(cfg)
	svc := &DuplicateIamRole{Client: iamClient}

	fmt.Println("  Get source role...")
	sourceRole := svc.GetRole(ctx, *sourceRoleName)

	fmt.Println("  Get inline policies...")
	inlinePolicies := svc.GetInlinePolicies(ctx, *sourceRoleName)

	fmt.Println("  Get managed policies...")
	managedPolicies := svc.GetManagedPolicies(ctx, *sourceRoleName)

	fmt.Println("  Create target role...")
	err = svc.CreateRole(ctx, sourceRole, *targetRoleName)
	if err != nil {
		log.Fatalf("unable to create role, %v", err)
		return
	}

	if len(inlinePolicies) > 0 {
		fmt.Println("  Attach inline policies...")
		err = svc.AttachInlinePolicies(ctx, *targetRoleName, inlinePolicies)
		if err != nil {
			log.Fatalf("unable to add inline policies, %v", err)
		}
	}

	if len(managedPolicies) > 0 {
		fmt.Println("  Attach managed policies...")
		err = svc.AttachManagedPolicies(ctx, *targetRoleName, managedPolicies)
		if err != nil {
			log.Fatalf("unable to add managed policies, %v", err)
		}
	}

	fmt.Println("  Done")
}
